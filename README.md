# Summary

Documentation, explanations, and external references for **BuzzCrate: Bingo in a Box**

## Presentation

The original rendition of this talk was delievered at [SouthEast LinuxFest 2019][self].
Despite several technical issues with the intended content, Will & Jason managed
to deliver a decent amount of the content. This was [recorded on video][self-video],
and the slides [can be found on Google Slides][slides].

## Cluster Bill of Materials

We will do our best to keep these up to date, and don't expect them to change much.

- [LibreCrate](bom/libre.md)
- [HardCrate](bom/hard.md)


## External links

Projects:

- [k3s](https://k3s.io)
- [RKE](https://github.com/rancher/rke)
- [Rancher](https://rancher.com/)

Linux Distributions:

- [Arch Linux ARM](http://archlinuxarm.org)

Single Board Computer:

- [LibreComputer](https://libre.computer)
- [Hardkernel](https://www.hardkernel.com)


Community:

- [SouthEast LinuxFest][self]



[self]: http://southeastlinuxfest.org/
[self-video]: https://www.youtube.com/watch?v=vRqG3NloJ6w
[slides]: https://docs.google.com/presentation/d/1qf0CTkzLQUBdcTwNTpPjwaYeEqhc3VQQ-bPFO3sIqNI/edit
